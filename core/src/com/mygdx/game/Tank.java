package com.mygdx.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;

public class Tank {
    private MyGdxGame game;
    private Texture texture;
    private Texture textureTurret;
    private float speed;
    private Vector2 position;
    private float x;
    private float y;
    private float angle;
    private float turretAngle;

    public Tank(MyGdxGame game) {
        this.game = game;
        this.texture = new Texture("tank.jpg");
        this.textureTurret = new Texture("simple_weapon.jpg");
        this.x = 110f;
        this.y = 110f;
        this.position = new Vector2(100, 100);
        this.speed = 100f;
        this.turretAngle = 0;
    }

    public void render(SpriteBatch batch) {
        batch.draw(texture, position.x - 32, position.y - 27, 32, 27, 64, 54, 1, 1, angle, 0, 0, 64, 54, false, false);
        batch.draw(textureTurret, position.x - 22, position.y - 16, 30, 16, 66, 32, 1, 1, turretAngle, 0, 0, 66, 32, false, false);
    }

    public void update(float dt) {
        CheckMoment(dt);
        float mx = Gdx.input.getX();
        float my = Gdx.graphics.getHeight() - Gdx.input.getY();
        float angleTwo = Utils.getAngle(position.x, position.y, mx, my);
        turretAngle = Utils.makeRotation(turretAngle, angleTwo, 360, dt);
        turretAngle = Utils.atangleToFormNegpiToPosPi(turretAngle);
        if (Gdx.input.isKeyJustPressed(Input.Keys.SPACE) || Gdx.input.justTouched()) {
            fire();
        }

    }

    public void CheckMoment(float dt) {
        if (Gdx.input.isKeyPressed(Input.Keys.LEFT) || Gdx.input.isKeyPressed(Input.Keys.A)) {
            position.x -= speed * dt;
            angle = 180.0f;
            return;
        }
        if (Gdx.input.isKeyPressed(Input.Keys.RIGHT) || Gdx.input.isKeyPressed(Input.Keys.D)) {
            position.x += speed * dt;
            angle = 0.0f;
            return;
        }
        if (Gdx.input.isKeyPressed(Input.Keys.UP) || Gdx.input.isKeyPressed(Input.Keys.W)) {
            position.y += speed * dt;
            angle = 90.0f;
            return;
        }
        if (Gdx.input.isKeyPressed(Input.Keys.DOWN) || Gdx.input.isKeyPressed(Input.Keys.S)) {
            position.y -= speed * dt;
            angle = 270.0f;
        }
    }

    public void fire() {

        float angleRad = (float) Math.toRadians(turretAngle);
        game.getBulletEmitter().activate(position.x, position.y, 1000.0f * (float) Math.cos(angleRad), 1000.0f * (float) Math.sin(angleRad));
    }

}

